$(function () {
    console.log('firing mode: normal')
    console.log('disarmed')
    console.log('lockout disabled')

    $('button#arm').click(function () {
        if ($(this).hasClass('active')) {
            console.log('disarmed')
            $('a#fire').attr("disabled", true);
        }
        if (!$(this).hasClass('active')) {
            console.log('armed')
            $('a#fire').attr("disabled", false);
        }
    });

    $('button#lockout').click(function () {
        if ($(this).hasClass('active')) {
            console.log('lockout disabled')
            $.getJSON('/locked');
        }
        if (!$(this).hasClass('active')) {
            console.log('lockout enabled')
            $.getJSON('/unlocked');
        }
    });

    $('button#mode').click(function () {
        if ($(this).hasClass('active')) {
            console.log('firing mode: normal')
            $('a#fire').removeClass('rapidfire');
        }
        if (!$(this).hasClass('active')) {
            console.log('firing mode: rapidfire')
            $('a#fire').addClass('rapidfire');
        }
    });

    $('button#shutdown').click(function () {
        // shutdown pi
        $.ajax({
            url: '/shutdown',
            success: function (data) {
                $('button#shutdown').disable();
                console.log(data)
            },
            error: function (error) {
                console.log(error)
            }
        })
    });

    $('a#fire').on('mousedown vmousedown taphold', function () {
        if ($(this).hasClass('rapidfire')) {
            console.log('firing rapidfire')
            $.getJSON('/rapidfire');
        } else {
            console.log('firing normal')
            $.getJSON('/fire');
        }
    }).on('mouseup mouseout vmouseup vmouseout', function () {
        $.getJSON('/unfire');
    });

    // prevent touch and hold
    window.oncontextmenu = function (event) {
        event.preventDefault();
        event.stopPropagation();
        return false;
    };

    // update stats
    setInterval(function () {
        $.ajax({
            url: '/stats',
            success: function (data) {
                var stats = JSON.parse(data);
                $('#tank-pressure').html('')
                $('#tank-pressure').html('coming soon!')
                $('#poof-duration').html('')
                $('#poof-duration').html(stats[0].pot1)
                $('#sleep-duration').html('')
                $('#sleep-duration').html(stats[0].pot2)
            },
            error: function (error) {
                console.log(error)
            }
        })
    }, 500);
});
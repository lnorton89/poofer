#----------------------------------------------------------------------------#
# Imports
#----------------------------------------------------------------------------#

from flask import Flask, render_template, request, jsonify, json
import logging
from logging import Formatter, FileHandler
from os import system
import RPi.GPIO as gpio
import smbus
import time
import sys
import pigpio
import math


#----------------------------------------------------------------------------#
# Serial Communication Config.
#----------------------------------------------------------------------------#

pi = pigpio.pi()
bus = smbus.SMBus(1)
address = 0x08


#----------------------------------------------------------------------------#
# App Config.
#----------------------------------------------------------------------------#

app = Flask(__name__)
app.config.from_object('config')


#----------------------------------------------------------------------------#
# Functions.
#----------------------------------------------------------------------------#

def read_stats():
    try:
        handle = pi.i2c_open(1, address)                   # open i2c bus
        #pi.i2c_write_byte(handle, 10)                      # send read command : TODO: ARDUINO SIDE
        #time.sleep(0.005)                                  # readings take up to 50ms, lets give it some time
        (count, byteArray) = pi.i2c_read_device(handle, 2) # vacuum up those bytes. second arg is array length
        pi.i2c_close(handle)                               # close the i2c bus
        h1 = byteArray[0]                                  # pot 1 value
        h2 = byteArray[1]                                  # pot 2 value
        data = [ { "pot1" : h1, "pot2" : h2 } ]            # create an array with our data

        return data
    except:
        return "An error has occured polling stats"



#----------------------------------------------------------------------------#
# Controllers.
#----------------------------------------------------------------------------#


@app.route('/')
def home():
    return render_template('pages/placeholder.home.html', stats=json.dumps(read_stats()))


@app.route('/unfire')
def unfire():
    bus.write_byte(address, 0)
    return "unfiring"


@app.route('/fire')
def fire():
    bus.write_byte(address, 1)
    return "firing"


@app.route('/rapidfire')
def rapidfire():
    bus.write_byte(address, 2)
    return "rapidfire"


@app.route('/unlocked')
def unlocked():
    bus.write_byte(address, 3)
    return "unlocked"


@app.route('/locked')
def locked():
    bus.write_byte(address, 4)
    return "locked"


@app.route('/stats')
def stats():
    return json.dumps(read_stats())


@app.route('/shutdown')
def shutdown():
    system("sudo shutdown -h -P now")
    return "shutdown initiated"


# Error handlers.

@app.errorhandler(500)
def internal_error(error):
        # db_session.rollback()
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404


if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter(
            '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')

#----------------------------------------------------------------------------#
# Launch.
#----------------------------------------------------------------------------#

# Default port:
if __name__ == '__main__':
    app.run()

/*
  Propane Poofer Switch Panel
*/
#include <Arduino.h>
#include <Wire.h>

// constants
const char ledPin = LED_BUILTIN;
const int buttonPin = 12;      // select the input pin for the button
const int relayPin = 11;       // select the pin for the relay
const int switchDownPin = 10;  // select the pin for switch in down position
const int switchUpPin = 9;     // select the pin for switch in up position
const int potPin = 2;          // select the pin for the potentiometer
const int potPin2 = 1;         // select the pin for the potentiometer 2

// variables will change:
int buttonState, switchUpState, switchDownState, inputVal, inputVal2, outputVal,
    outputVal2, number, dataNo;
bool lockout;

int fire(bool ifSerial) {
  if (lockout && ifSerial == false) {
    Serial.println("blocked");
    return 0;
  }
  Serial.println(ifSerial);
  digitalWrite(relayPin, HIGH);
  digitalWrite(ledPin, HIGH);
}

int rapidfire(bool ifSerial) {
  inputVal = analogRead(potPin);
  inputVal2 = analogRead(potPin2);
  outputVal = map(inputVal, 0, 1023, 25, 150);
  outputVal2 = map(inputVal2, 0, 1023, 150, 25);

  fire(ifSerial);
  delay(outputVal);
  unfire();
  delay(outputVal2);
}

int unfire() {
  digitalWrite(relayPin, LOW);
  digitalWrite(ledPin, LOW);
}

void setup() {
  // init serial comm
  Serial.begin(9600);
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onReceive(receiveData);  // register event for receieving data over i2c
  Wire.onRequest(sendData);     // register event for sending data over i2c
  Serial.println("ready!");

  // setup our pins
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(switchUpPin, INPUT_PULLUP);
  pinMode(switchDownPin, INPUT_PULLUP);
}

void loop() {
  // read the state of the inputs
  buttonState = digitalRead(buttonPin);
  switchUpState = digitalRead(switchUpPin);
  switchDownState = digitalRead(switchDownPin);

  // check if the pushbutton is pressed. If it is, the buttonState is LOW:
  if (buttonState == LOW) {
    // rapid fire (3 way switch in up position)
    if (switchUpState == LOW) {
      bool isSerial = false;
      rapidfire(isSerial);
      Serial.println("firing from button rapidfire");
    }

    // not used (3 way switch in down position)
    if (switchDownState == LOW) {
      // do nothing
    }

    // normal mode (3 way switch in middle position)
    if (switchUpState == HIGH && switchDownState == HIGH) {
      bool isSerial = false;
      fire(isSerial);
      Serial.println("firing from button normally");
    }
  }

  else {
    unfire();
  }

  while (dataNo == 1) {
    bool isSerial = true;
    fire(isSerial);
    Serial.println("firing from i2c");
  }

  while (dataNo == 2) {
    bool isSerial = true;
    rapidfire(isSerial);
    Serial.println("rapidfiring from i2c");
  }

  delay(15);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveData(int byteCount) {
  while (Wire.available()) {
    number = Wire.read();
    if (number == 0) {
      dataNo = 0;
      unfire();
    }
    if (number == 1) {
      dataNo = 1;
    }
    if (number == 2) {
      dataNo = 2;
    }
    if (number == 3) {
      lockout = true;
      Serial.println("phsyical controls locked out");
    }
    if (number == 4) {
      lockout = false;
      Serial.println("phsyical controls unlocked");
    } else {
      // do nothing
    }
  }
}

void sendData() {
  int var1 = map(analogRead(potPin), 0, 1023, 25, 150);   // pot1
  int var2 = map(analogRead(potPin2), 0, 1023, 150, 25);  // pot2
  byte buf [2] = { var1, var2 };                          // array of values as bytes
  Wire.write (buf, sizeof buf);                           // send response
}
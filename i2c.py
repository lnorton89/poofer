import smbus
import time
import sys
import struct

bus = smbus.SMBus(1)
address = 0x08


def readNumber():
    print(bus.read_byte(address))

def main():
    while 1:
        readNumber()
        time.sleep(1)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print 'Interrupted'
        sys.exit(0)
